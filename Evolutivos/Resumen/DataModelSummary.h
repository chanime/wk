//
//  DataModelSummary.h
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 03/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef DataModelSummary_h
#define DataModelSummary_h

typedef enum : NSUInteger {
    CreditCell = 0,
    CreditCellAux,
    PayCell,
    GraphicCell,
    BtnCell,
    ExpensesSummaryCell
} SummaryDataCell;

typedef enum : NSUInteger{
    LineCredit = 0,
    AvaliableCredit,
    WillingCredit,
    PendingConfirmation
} CreditInfoType;

typedef enum : NSUInteger{
    ToPay = 0,
    OthersPayments,
    PayMode
} PayInfoType;

#endif /* DataModelSummary_h */
