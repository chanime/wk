//
//  NewModalView.m
//  popular
//
//  Created by javi on 02/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import "NewModalView.h"

@implementation NewModalView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self fillFrame];
    [self initView];
    [self fadeIn:self.modalView];
}

-(void)initView{
    _modalView.layer.cornerRadius = 10.0f;
    _modalView.transform = CGAffineTransformMakeTranslation(0, 15.0f);
    _linkButton.hidden = YES;
    [_linkButton setTitleColor:[UIColor popularDarkBlue] forState:UIControlStateNormal];
    _infoTextView.textContainer.lineFragmentPadding = 0;
    _infoTextView.textContainerInset = UIEdgeInsetsZero;
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 7;
    _infoTextView.attributedText = [[NSAttributedString alloc]
                                    initWithString:@"my text"
                                    attributes:@{NSParagraphStyleAttributeName : style, NSFontAttributeName: [UIFont fontWithName:@"Geomanist-Light" size:20.0f]}];
}

-(void)fillFrame{
    CGRect frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
    self.frame = frame;
}
//VALIDATIONS
-(void) configureValidationModalWithOption:(int)option withText:(NSString*)text{
    switch (option){
        case FINAL_BILING:
            [self configureFinalBiling];
            break;
        case CHANGE_FORM_PAY_AFTER_PAY_DAY:
            [self configureFormPayAfter];
            break;
        case CHANGE_FORM_PAY_BEFORCE_PAY_DAY:
            [self configureFormPayBeforce];
            break;
        case RETURNED_RECEIPT:
            [self configureReturnedReceipt:text];
            break;
        case EPP1:
            [self configurePostponePurchase];
            break;
        case DD_SPLIT_ADITIONAL:
            [self configureDDAditional];
            break;
        case DD_SPLIT_WITHOUT_ADITIONAL:
            [self configureDDWithoutAditional:text];
            break;
        case PENDING_CONFIRMATION:
            [self configurePendingConfirmation];
            break;
    }
}
//AMOUNT TO PAY
-(void) configureBilingPeriodModalWithOption: (int) option withText:(NSString*)text{
    switch (option){
        case BILING_DUE_DATE_SCENARIO_1:
            [self configureBilingDueScenerio1:text];
            break;
        case BILING_DUE_DATE_SCENARIO_2:
            [self configureBilingDueScenerio2];
            break;
        case DUE_DATE_NEXT_BILING_SCENARIO_1:
            [self configureDueBilingScenario1:text];
            break;
        case DUE_DATE_NEXT_BILING_SCENARIO_2:
            [self configureDueBilingScenario2];
            break;
    }
}


//COP_BS_AMOUNT_ TO_PAY_INFO_1
//DISPUESTO A CIERRE
-(void)configureFinalBiling{
      self.modalValidation = FINAL_BILING;
     [self configureModalTitle: NSLocalizedString(@"COP_BS_CONSUMED_AT_CLOSE",@"COP_BS_CONSUMED_AT_CLOSE") info:NSLocalizedString(@"COP_BS_CONSUMED_AT_CLOSE_INFO",@"COP_BS_CONSUMED_AT_CLOSE_INFO") withoutLink:NO];
}
//-VALIDATIONS 
//cambiar forma de pago antes de Fecha de Facturación
-(void) configureFormPayAfter{
    [self configureModalTitle: NSLocalizedString(@"COP_BS_CHANGE_PAYMENT_MODAL",@"COP_BS_CHANGE_PAYMENT_MODAL") info:[NSString stringWithFormat: NSLocalizedString(@"COP_BS_CHANGE_PAYMENT_OPTION_MODAL",@"COP_BS_CHANGE_PAYMENT_OPTION_MODAL"),[self giveMeDate:@"06/6/2018 10:45"],@"Total",[self giveMeDate:@"06/7/2018 10:45"]]withoutLink:YES];
}
//cambiar forma de pago después de Fecha Facturación
-(void) configureFormPayBeforce{
    [self configureModalTitle:NSLocalizedString(@"COP_BS_CHANGE_PAYMENT_MODAL",@"COP_BS_CHANGE_PAYMENT_MODAL") info:[NSString stringWithFormat: NSLocalizedString(@"COP_BS_CHANGE_PAYMENT_OPTION_MODAL_1",@"COP_BS_CHANGE_PAYMENT_OPTION_MODAL_1"),[self giveMeDate:@"21/5/2018 10:45"]]withoutLink:YES];
}
//RECIBO DEVUELTO
-(void)configureReturnedReceipt:(NSString*)text{
    self.modalValidation = RETURNED_RECEIPT;
    [self configureModalTitle:NSLocalizedString(@"COP_BS_RETURNED_RECEIPT",@"COP_BS_RETURNED_RECEIPT") info:text withoutLink:NO];
}
//-EPP APLAZAR COMPRA
-(void) configurePostponePurchase{
    [self configureModalTitle:NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_1",@"COP_BS_AMOUNT_TO_PAY_1") info:[NSString stringWithFormat: NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_INFO_2",@"COP_BS_AMOUNT_TO_PAY_INFO_2"),[self giveMeDate:@"02/6/2018 10:45"]]withoutLink:YES];
}
//DD
-(void)configureDDAditional{
     [self configureModalTitle:NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_1",@"COP_BS_AMOUNT_TO_PAY_1") info:[NSString stringWithFormat: NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_MODAL_INFO_1",@"COP_BS_AMOUNT_TO_PAY_MODAL_INFO_1"),[self giveMeDate:@"01/7/2018 10:45"]]withoutLink:YES];
}

-(void)configureDDWithoutAditional:(NSString*)text{
    [self configureModalTitle:NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_1",@"COP_BS_AMOUNT_TO_PAY_1") info:text withoutLink:YES];
}
//PENDIENTE CONFIRMAR
-(void)configurePendingConfirmation{
     [self configureModalTitle: NSLocalizedString(@"COP_BS_PENDING_CONFIRMATION",@"COP_BS_PENDING_CONFIRMATION") info:NSLocalizedString(@"COP_BS_PENDING_CONFIRMATION_INFO",@"COP_BS_PENDING_CONFIRMATION_INFO") withoutLink:YES];
}
//-AMOUNT TO PAY
//a)ENTRE FECHA DE FACTURACION Y DE CIERRE "NO HAS PAGADO"
//-SCENARIO 1 Total, Mínimo, Porcentaje, Fijo , sin pagar la totalidad del crédito
-(void)configureBilingDueScenerio1:(NSString*)text{
    [self configureModalTitle:NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_1",@"COP_BS_AMOUNT_TO_PAY_1") info:nil withoutLink:YES];
    [self configTextView:text];
}
//-SCENARIO2  Porcentaje, Fijo ¿ Total, Mínimo ? con el crédito pagado en su totalidad
-(void)configureBilingDueScenerio2{
[self configureModalTitle:NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_1",@"COP_BS_AMOUNT_TO_PAY_1") info:NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_INFO_1",@"COP_BS_AMOUNT_TO_PAY_INFO_1") withoutLink:YES];

}

//b)ENTRE FECHA DE CIERRE Y DE FACTURACIÓN "HAS PAGADO"
//-SCENARIO 1 todos los pagadores. -Pago adicional sin llegar al total -Sin pago adicional.
-(void)configureDueBilingScenario1:(NSString*)text{
    [self configureModalTitle:NSLocalizedString(@"COP_BS_PAID_RECIPT",@"COP_BS_PAID_RECIPT") info:text withoutLink:YES];

}
//-SCENARIO2 todos los pagadores. -Pago adicional igual al gasto de crédito  -Pago adicional superior al gasto de crédito
-(void)configureDueBilingScenario2{
    [self configureModalTitle:NSLocalizedString(@"COP_BS_PAID_RECIPT",@"COP_BS_PAID_RECIPT") info:NSLocalizedString(@"COP_BS_PAID_RECIPT_INFO_1",@"COP_BS_PAID_RECIPT_INFO_1") withoutLink:YES];
}

-(void) configureModalTitle:(NSString *) title info:(NSString *) infoText withoutLink:(BOOL) hideLink{
    
    _titleLabel.text = title;
    _infoTextView.text = infoText;
    if(hideLink){
        
        _linkButton.hidden = hideLink;
        _linkButtonHeightConstraint.constant = 0.0f;
        
    }else{
        
        _linkButton.hidden = hideLink;
        _linkButtonHeightConstraint.constant = 50.0f;
        [self configureButton];
        
    }
    
}

-(void)configureButton{
    
    switch (_modalValidation) {
        case RETURNED_RECEIPT:{
            //[NSString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]
            [_linkButton setTitle:NSLocalizedString(@"COP_BS_RETURNED_RECEIPT_BUTTON",@"COP_BS_RETURNED_RECEIPT_BUTTON") forState:UIControlStateNormal];
            //ponerle acción delegado ?¿? -> PAGAR CRÉDITO UTILIZADO
        }
            break;
        case FINAL_BILING:
             [_linkButton setTitle:NSLocalizedString(@"COP_BS_EXTRACT_MOVEMENTS_LINK",@"COP_BS_EXTRACT_MOVEMENTS_LINK") forState:UIControlStateNormal];
            //ponerle acción delegado ?¿? -> VER MOVIMIENTOS
            break;
        default:
            break;
    }
}


-(void)configTextView:(NSString*)text {
    
    NSString * linkString = NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_LINK",@"COP_BS_AMOUNT_ TO_PAY_LINK");
    _infoTextView.text = [NSString stringWithFormat: NSLocalizedString(@"COP_BS_AMOUNT_TO_PAY_MODAL_INFO_2",@"COP_BS_AMOUNT_TO_PAY_MODAL_INFO_2"),[self giveMeDate:@"06/6/2018 10:45"],@"500",[self giveMeDate:@"06/7/2018 10:45"],linkString];
    _infoTextView.text = text;//[NSString stringWithFormat:self.infoTextView.text, linkString];
    [self configureLink:linkString withURL:@"" inTextView:self.infoTextView];
    _infoTextView.delegate = self;
}

-(void) configureLink:(NSString *)link withURL:(NSString *)url inTextView:(UITextView *)textView {
    
    NSString * text = textView.text;
    NSMutableAttributedString * attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:textView.attributedText];
    [attributedText addAttribute:NSLinkAttributeName value:url range:[text rangeOfString:link]];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Geomanist-WZ-Book" size:20.0f] range:[textView.text rangeOfString:link]];
    textView.attributedText = attributedText;
    textView.linkTextAttributes = @{ NSForegroundColorAttributeName : [UIColor popularDarkBlue] };
   
}

-(NSString *) giveMeDate:(NSString *) dateToParse{
    
    NSDateFormatter *formatterA = [[NSDateFormatter alloc] init];
    formatterA.dateFormat = @"dd-MM-yyyy HH:mm";
    NSDate * timeService = [formatterA dateFromString:dateToParse];
    NSDateFormatter *dft = [[NSDateFormatter alloc] init];
    [dft setLocalizedDateFormatFromTemplate:@"MMMM d"];
    NSString * resultDate =  [dft stringFromDate:timeService];
    return resultDate;
    
}
#pragma mark - TextView Delegate
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    if( self.summaryDelegate && [self.summaryDelegate respondsToSelector:@selector(solicitedGotoChangePayMode)] ){
        [self.summaryDelegate solicitedGotoChangePayMode];
    }
    [self fadeOut:self.modalView];
    return YES;
}

- (IBAction)closeButton:(id)sender {
    [self fadeOut:self.modalView];
}

- (IBAction)linkButtonAction:(id)sender {
    if( self.delegate && [self.delegate respondsToSelector:@selector(lunchShowExtract)] ){
        [self.delegate lunchShowExtract];
    }
    else if( self.summaryDelegate && [self.summaryDelegate respondsToSelector:@selector(gotoPayDebitFromModal)] ){
        [self.summaryDelegate gotoPayDebitFromModal];
    }
    [self fadeOut:self.modalView];
}

@end
