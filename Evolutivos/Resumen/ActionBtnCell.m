//
//  ActionBtnCell.m
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 05/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import "ActionBtnCell.h"

@implementation ActionBtnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
