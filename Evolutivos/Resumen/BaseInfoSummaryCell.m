//
//  BaseInfoSummaryCell.m
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 03/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import "BaseInfoSummaryCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation BaseInfoSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.isList = false;
    self.isHyperLinkCreated = false;
    self.showInfoType = None;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Methods
-(void)configure:(NSString *)desc withTitle:(NSString*)title withSubtitle:(NSString*)subtitle withImport:(NSString*)import highLight:(NSArray*)search{}

#pragma mark - IBActions
-(IBAction)show:(id)sender{
    self.isShowed = !self.isShowed;
    if( self.iconAction != nil ){
        [self rotateImageView];
    }
}

-(IBAction)showInfo:(id)sender{
    return;
    /*if( self.showInfoType == None ){
        return;
    }
    if(self.baseDelegate && [self.baseDelegate respondsToSelector:@selector(solicitedInfoToShow:)]){
        [self.baseDelegate solicitedInfoToShow:self];
    }*/
}

#pragma mark - Animate Methods
- (void)rotateImageView{
    CGFloat r = M_PI;
    if( !self.isShowed ){
        r = 0;
    }
    [self.iconAction setTransform:CGAffineTransformRotate(self.imageView.transform, r)];
}

#pragma mark - Add advantanges
-(void)addHyperLink:(NSString*)tmpSearchString{
    if( self.isHyperLinkCreated ){
        [[self.subtitle viewWithTag:1001] removeFromSuperview];
    }
    NSInteger lineCount = 0;
    UILabel* tmp = self.subtitle;
    [tmp setUserInteractionEnabled:true];
    [tmp setLineBreakMode:NSLineBreakByWordWrapping];
    [tmp sizeToFit];
    CGSize textSize = CGSizeMake(tmp.frame.size.width, MAXFLOAT);
    int rHeight = (int) lroundf([tmp sizeThatFits:textSize].height);
    int charSize = (int) lroundf(tmp.font.lineHeight);
    lineCount = rHeight/charSize;
    NSRange range = [tmp.text rangeOfString:tmpSearchString];
    UIFont *font = tmp.font;
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    CGFloat w = 0;
    CGFloat ww = 0;
    CGFloat y = 0;
    NSInteger currentLine = 0;
    for (uint x = 0; x<tmp.text.length; x++) {
        NSString* character = [tmp.text substringWithRange:NSMakeRange(x, 1)];
        CGSize fontSize = [character sizeWithAttributes:userAttributes];
        w += fontSize.width;
        if( x >= range.location && x <= range.location + range.length ){
            ww += fontSize.width;
        }
        if( w >= textSize.width ){
            w = w - textSize.width;
            currentLine++;
            y += rHeight/lineCount;
        }
        if( x == range.location + range.length ){
            CGFloat xf = ww;
            CGFloat xi = w - xf;
            if( xi < 0 ){
                xi = 0;
            }
            if( [tmpSearchString isEqualToString:NSLocalizedString(@"COP_CHANGE_PAY_MODE_LINK", @"COP_CHANGE_PAY_MODE_LINK")] ){
                xi = 50;
                y += adjustSizeLinkBtn*4;
            }
            
            AdaptativeButton* btn = [[AdaptativeButton alloc] initWithFrame:CGRectMake(xi - adjustSizeLinkBtn, y-(adjustSizeLinkBtn*1.5), xf + (adjustSizeLinkBtn*2), (rHeight/lineCount)+(adjustSizeLinkBtn*2))];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn setTag:tagForActBtn];
            //Debug():
            //[btn setBackgroundColor:[UIColor colorWithRed:33/255 green:33/255 blue:33/255 alpha:.2]];
            [btn addTarget:self action:@selector(gotoMovements:) forControlEvents:UIControlEventTouchUpInside];
            [tmp addSubview:btn];
            self.isHyperLinkCreated = true;
            break;
        }
    }
}

-(void)gotoMovements:(id)sender{
    if(self.baseDelegate){
        switch (self.goTo) {
            case GotoMovements:
                if( [self.baseDelegate respondsToSelector:@selector(solicitedGotoMovements)] ){
                    [self.baseDelegate solicitedGotoMovements];
                }
                break;
            case GotoChangePayMode:
                if( [self.baseDelegate respondsToSelector:@selector(solicitedGotoChangePayMode)] ){
                    [self.baseDelegate solicitedGotoChangePayMode];
                }
                break;
            default:
                break;
        }
    }
}

@end
