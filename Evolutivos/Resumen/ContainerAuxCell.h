//
//  ContainerAuxCell.h
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 02/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContainerCell.h"
#import "SortedCard.h"
#import "BillingExpensesResponseRest.h"

@interface ContainerAuxCell : ContainerCell<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet NSLayoutConstraint* hIco;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* wIco;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* betweenIco;

@property(nonatomic, strong) NSArray* items;

-(void)addItems:(NSArray*)items;
-(void)addCard:(SortedCard*)card withBilling:(BillingExpensesResponseRest*)rest;
-(void)registerTypeCells:(NSString*)myClass withId:(NSString*)myClassId;

@end
