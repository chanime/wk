//
//  GraphicView.h
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 07/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdaptativeLabel.h"
#import "UILabel+Utils.h"

@protocol GraphicProtocol<NSObject>

-(void)updateSize;

@optional

-(void)solicitedInfoToParent;
-(void)solicitedInfoToParentForPayImport;

@end

@protocol GraphicCellParentProtocol<NSObject>

@optional

-(void) adviseForEndAnimation;
-(void) adviseForRepeatAnimation;

@end

@interface GraphicView : UIView

@property(nonatomic, weak) IBOutlet AdaptativeLabel* pastPeriodLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* actualPeriodLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* readyToCloseLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* readyToCloseImportLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* nextCloseLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* beginPeriodDateLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* beginActualPeriodDateLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* endPeriodDateLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* todayLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* payTitleLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* payDateLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* payImportLb;
@property(nonatomic, weak) IBOutlet AdaptativeLabel* beginLb;
@property(nonatomic, weak) IBOutlet UIView* bar;
@property(nonatomic, weak) IBOutlet UIView* progress;
@property(nonatomic, weak) IBOutlet UIView* beginDateView;
@property(nonatomic, weak) IBOutlet UIView* endDateView;
@property(nonatomic, weak) IBOutlet UIView* peridoToCloseView;
@property(nonatomic, weak) IBOutlet UIView* peridoBeforeView;
@property(nonatomic, weak) IBOutlet UIView* maskEndPeriodView;
@property(nonatomic, weak) IBOutlet UIImageView* lineVertLeftBegin;
@property(nonatomic, weak) IBOutlet UIImageView* beginIcon;
@property(nonatomic, weak) IBOutlet UIImageView* endIcon;
@property(nonatomic, weak) IBOutlet UIImageView* lineLeft;
@property(nonatomic, weak) IBOutlet UIImageView* lineRight;
@property(nonatomic, weak) IBOutlet UIImageView* lineVertRight;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* leftXPos;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* posXTodayLb;
//THID PROPERTY INFORM US WHERE GO TO PUT THE VERT LINE FOR THE INFO ABOUT READY TO CLOSE.
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* posXInfoReadyToClose;
//THID PROPERTY CAN ADJUT INFO WHEN THE INFO OVERLOAD THE PARENT VIEW.
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* PosXInfoContentReadyToPay;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* heightDatePayContent;

@property(nonatomic, weak) id<GraphicProtocol>delegate;
@property(nonatomic, weak) id<GraphicCellParentProtocol>cellDelegate;

-(void)animatePoint;
-(void)fillInfo:(NSString*)dateBegin withDateEnd:(NSString*)dateEnd withHaveReceipt:(BOOL)haveReceipt withPeriod:(CGFloat)period withHaveInfoReadyToClose:(BOOL)haveInfoReadyToClose withDayPeriod:(NSInteger)day withReturnReceipt:(BOOL)returnReceipt withImportA:(NSString*)ia withImportB:(NSString*)ib widthDateBeginActualPeriod:(NSString*)dateBeginActual withAdvanceGraph:(NSNumber*)advanceBar withPayDate:(NSString*)payDate withDueDate:(BOOL)dueDatePast withImporToPay:(NSString*)import withSameDay:(BOOL)sameDay;
-(void)resetAnimation;

@end
