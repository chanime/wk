//
//  ContainerCell.h
//  celdas_hoy
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 02/01/2019.
//  Copyright © 2018 JOSE VICENTE GARCIA CHAHUAN. All rights reserved.
//

#import "BaseInfoSummaryCell.h"

static CGFloat const defaultHeightAuxTb = 8;
static CGFloat const footerPay = 75;
static CGFloat const adjustFirstLoad = 40;
static CGFloat const cornerTb = 5.0f;
static CGFloat const lineHeightText = 5.0f;

#define ATTENTION_COLOR [UIColor colorWithRed:255.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1];
#define WARNING_COLOR [UIColor colorWithRed:252.0/255.0 green:240.0/255.0 blue:223.0/255.0 alpha:1];

@class ContainerCell;

@protocol ContainerProtocol <NSObject>

@optional

-(void)canShow:(ContainerCell*)cell;
-(void)showListCatego:(NSString*)name;

@end

@interface ContainerCell : BaseInfoSummaryCell

@property(weak, nonatomic) IBOutlet UILabel* ln;
@property(weak, nonatomic) IBOutlet UITableView* auxTable;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint* heightAuxTable;

#pragma mark - PROPERTIES FOR SHOW CONTENT
@property (nonatomic, weak) id<ContainerProtocol> delegate;
@property(nonatomic, assign) BOOL haveScroll;

@end
