//
//  ContainerAuxCell.m
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 02/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import "ContainerAuxCell.h"
#import "CreditAuxCell.h"
#import "WZKMovementSummaryCell.h"
#import "Catego.h"
#import "AccountSummaryMap.h"
#import "SummaryTableViewCell.h"

@interface ContainerAuxCell()

@property(nonatomic, assign) BOOL haveOtherCell;
@property(nonatomic, strong) NSMutableArray *categos;
@property(nonatomic, strong) SortedCard* card;
@property(nonatomic, strong) BillingExpensesResponseRest* infoBilling;

@end

@implementation ContainerAuxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.heightAuxTable.constant = defaultHeightAuxTb;
    self.auxTable.hidden = true;
    self.auxTable.rowHeight = UITableViewAutomaticDimension;
    [self.auxTable registerNib:[UINib nibWithNibName:@"CreditAuxCell" bundle:nil] forCellReuseIdentifier:@"CreditAuxCellID"];
    _haveOtherCell = false;
}

-(void)configure:(NSString *)desc withTitle:(NSString*)title withSubtitle:(NSString*)subtitle withImport:(NSString *)import highLight:(NSArray *)search{
    [super configure:desc withTitle:title withSubtitle:subtitle withImport:import highLight:search];
}

-(void)registerTypeCells:(NSString*)myClass withId:(NSString*)myClassId{
    [self getCategos];
    if( self.categos.count == 0 ){
        self.iconAction.hidden = true;
        self.actionBtn.hidden = self.iconAction.hidden;
    }
    else{
        NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"value" ascending:NO comparator:^NSComparisonResult(id obj1, id obj2)  {
            if(obj1 == nil){
                //customize here
                return NSOrderedDescending;
            }else if(obj2 == nil){
                return NSOrderedAscending;
            }else{
                return [obj1 compare:obj2];
            }
        }];
        [self.categos sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    }
    [self addItems:self.categos];
    [self.auxTable registerNib:[UINib nibWithNibName:myClass bundle:nil] forCellReuseIdentifier:myClassId];
    _haveOtherCell = true;
    self.haveScroll = true;
    [self.auxTable reloadData];
}

#pragma mark - Create object
-(void) getCategos{
    _categos = [[NSMutableArray alloc]init];
    for (AccountSummaryMap * tempObject in ((SummaryTableViewCell*)self.delegate).currentCard.accountSummaryList) {
        
        Catego *catego = [Catego new];
        
        catego.name = tempObject.category;
        
        NSNumber *negativo = [NSNumber numberWithDouble:tempObject.monto ];
        catego.value = negativo;
        
        NSNumber *valorAbs  = [NSNumber numberWithDouble:fabs(tempObject.monto)];
        
        if (valorAbs.doubleValue  >0)
        {
            [self.categos addObject:catego];
        }
        
    }
}

-(IBAction)show:(id)sender{
    if( self.type == ExpensesSummaryCell && self.items.count == 0 ){
        return;
    }
    [super show:sender];
    self.heightAuxTable.constant = 1000;
    self.auxTable.delegate = self;
    self.auxTable.dataSource = self;
    //[self.auxTable reloadData];
    if( !self.isShowed ){
        self.auxTable.hidden = true;
        self.heightAuxTable.constant = defaultHeightAuxTb;
    }
    else{
        self.auxTable.hidden = false;
    }
    [self.lineMax layoutIfNeeded];
    [self.delegate canShow:self];
}

#pragma mark - Methods
-(void)addItems:(NSArray *)items{
    self.items = items;
}

-(void)addCard:(SortedCard*)card withBilling:(BillingExpensesResponseRest*)rest{
    _card = card;
    _infoBilling = rest;
}

-(float)getProgress:(NSInteger)total andParcial:(NSInteger)parcial{
    float progress = (float)parcial/total; //self.items.count;
    return progress;
}

#pragma mark - Tableview methods
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if( _haveOtherCell ){
        WZKMovementSummaryCell *cellSummary = cellSummary=[tableView dequeueReusableCellWithIdentifier:@"movementSummaryCell"];
        cellSummary.selectionStyle = UITableViewCellAccessoryNone;
        if ( self.categos.count ) {
            Catego *catego = [self.categos objectAtIndex:indexPath.row];
            [cellSummary initCellwithCategory:catego.name andImage: [Utils imageToCategorySmall:catego.name] andAmount:[NSString stringWithFormat:@"%@€",catego.value] andProgres:[self getProgress:[((SummaryTableViewCell*)self.delegate).currentCard.movementsAmount integerValue] andParcial:[catego.value integerValue]]];
        }
        if(indexPath.row == 0){
            [cellSummary.circleImagen .layer setBorderColor:[UIColor colorWithHexString:@"00c6c5"].CGColor];
            [cellSummary.progressBar setProgressTintColor:[UIColor colorWithHexString:@"00c6c5"]];
        }else if(indexPath.row == 1){
            [cellSummary.circleImagen .layer setBorderColor:[UIColor colorWithHexString:@"b5d480"].CGColor];
            [cellSummary.progressBar setProgressTintColor:[UIColor colorWithHexString:@"b5d480"]];
        }else if(indexPath.row == 2){
            [cellSummary.circleImagen .layer setBorderColor:[UIColor colorWithHexString:@"5bb8e5"].CGColor];
            [cellSummary.progressBar setProgressTintColor:[UIColor colorWithHexString:@"5bb8e5"]];
        }else if(indexPath.row == 3){
            [cellSummary.circleImagen .layer setBorderColor:[UIColor colorWithHexString:@"eeb464"].CGColor];
            [cellSummary.progressBar setProgressTintColor:[UIColor colorWithHexString:@"eeb464"]];
        }else if(indexPath.row == 4){
            [cellSummary.circleImagen .layer setBorderColor:[UIColor colorWithHexString:@"b382d0"].CGColor];
            [cellSummary.progressBar setProgressTintColor:[UIColor colorWithHexString:@"b382d0"]];
        }else{
            [cellSummary.circleImagen .layer setBorderColor:[UIColor colorWithHexString:@"152a69"].CGColor];
            [cellSummary.progressBar setProgressTintColor:[UIColor colorWithHexString:@"152a69"]];
        }
        if( indexPath.row == self.items.count - 1 ){
            [cellSummary.downLine setHidden:TRUE];
        }
        return cellSummary;
    }
    else{
        CreditAuxCell* cell = (CreditAuxCell*) [tableView dequeueReusableCellWithIdentifier:@"CreditAuxCellID" forIndexPath:indexPath];
        cell.title.text = [NSString stringWithFormat:NSLocalizedString(@"COP_BS_CONSUMED_BALANCE_1", @"COP_BS_CONSUMED_BALANCE_1"),self.items[0]];
        [cell.title highlightText:[UIColor colorWithRed:240.0/255.0 green:181.0/255.0 blue:97.0/255.0 alpha:1] withSearch:@[self.items[0], NSLocalizedString(@"COP_BS_TODAY_1", @"COP_BS_TODAY_1")]];
        cell.subtitle.text = NSLocalizedString(@"COP_BS_CONSUMED_BALANCE_2", @"COP_BS_CONSUMED_BALANCE_2");
        cell.import.text = [Utils formtarCurrency:_infoBilling.dispuestoPeriodoActual.floatValue];
        if( indexPath.row == 1 ){
            cell.lineMax.hidden = true;
            cell.import.text = [Utils formtarCurrency:_infoBilling.dispuestoPeriodoAnterior.floatValue];
            cell.title.text = [NSString stringWithFormat:NSLocalizedString(@"COP_BS_CONSUMED_BALANCE_3", @"COP_BS_CONSUMED_BALANCE_3"),self.items[1], self.items[0]];
            [cell.title highlightText:[UIColor colorWithRed:240.0/255.0 green:181.0/255.0 blue:97.0/255.0 alpha:1] withSearch:@[self.items[1], self.items[0]]];
            cell.subtitle.text = NSLocalizedString(@"COP_PAST_MONTH", @"COP_PAST_MONTH");
        }
        else{
            cell.lineMax.hidden = false;
        }
        [cell.import decreaseDecimalWithFontSize:cell.import.font.pointSize-4 andFont:@"Geomanist-WZ-Light"];
        [cell.subtitle lineHeight:lineHeightText];
        return cell;
    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if( _haveOtherCell ){
        return 65;
    }
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if( _haveOtherCell ){
        return 65;
    }
    return UITableViewAutomaticDimension;
}

-(void) tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void) tableView:(UITableView *) tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if( self.haveOtherCell ){
        WZKMovementSummaryCell * auxcell = (WZKMovementSummaryCell*) cell;
        if( auxcell.isAnimating ){
            return;
        }
        auxcell.isAnimating = true;
        [auxcell.progressBar setAlpha:0];
        [auxcell.progressBar setProgress:0 animated:NO];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.025 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:4
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear animations:^{
                                    [auxcell.progressBar setProgress:0 animated:NO];
                                } completion:^(BOOL finished){
                                    if(finished){
                                        [auxcell.progressBar setAlpha:1];
                                        [UIView animateWithDuration:2
                                                              delay:0
                                                            options:UIViewAnimationOptionCurveLinear animations:^{
                                                                [auxcell.progressBar setProgress:fabsf(auxcell.progress) animated:YES];
                                                            } completion:^(BOOL finished){
                                                                
                                                            }];
                                    }
                                }];
        });
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if( self.haveOtherCell ){
        Catego *catego = [self.categos objectAtIndex:indexPath.row];
        [self.delegate showListCatego:catego.name];
        NSString * eventCategory = [NSString stringWithFormat:@"%@ | %@" ,NSLocalizedString(@"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_MAP_KEY", @"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_MAP_KEY" ),
                                    NSLocalizedString(@"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_INDEX", @"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_INDEX")];
        [TealiumHelper trackEventWithTitle: NSLocalizedString(@"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_MAP_KEY", @"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_MAP_KEY")
                               dataSources:@{NSLocalizedString(@"TL_CAT_MAIN_KEY", @"TL_CAT_MAIN_KEY"):
                                                 NSLocalizedString(@"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_MAP_KEY", @"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_MAP_KEY"),
                                             NSLocalizedString(@"TL_SUB_CAT_MAN_KEY",@"TL_SUB_CAT_MAN_KEY"):
                                                 NSLocalizedString(@"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_INDEX", @"TL_EV_CAT_ACCOUNT_SUMMARY_TODAY_INDEX"),
                                             NSLocalizedString(@"TL_EVENT_ACTION_TITLE", @"TL_EVENT_ACTION_TITLE"):
                                                 NSLocalizedString(@"TL_EV_MY_CARDS_SUBMENU_TODAT", @"TL_EV_MY_CARDS_SUBMENU_TODAT"),
                                             NSLocalizedString(@"TL_EVENT_ACTION_LABEL", @"TL_EVENT_ACTION_LABEL"):
                                                 [NSString stringWithFormat:NSLocalizedString(@"TL_EV_LABEL_AS_CLICK_CATEGORY_KEY", @"TL_EV_LABEL_AS_CLICK_CATEGORY_KEY"), catego.name],
                                             NSLocalizedString(@"TL_EVENT_CATEGORY", @"TL_EVENT_CATEGORY"): eventCategory}];
    }
}

@end
