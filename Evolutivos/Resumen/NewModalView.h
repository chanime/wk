//
//  NewModalView.h
//  popular
//
//  Created by javi on 02/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Effect.h"
#import "AdaptativeLabel.h"
#import "AdaptativeButton.h"
#import "AdaptativeTextview.h"
#import "BaseInfoSummaryCell.h"

@protocol NewModalViewDelegate<NSObject>

@optional

-(void)lunchShowExtract;

@end


@interface NewModalView : UIView <UITextViewDelegate>

typedef NS_ENUM(NSInteger, validation) {
    
   RETURNED_RECEIPT, CHANGE_FORM_PAY_AFTER_PAY_DAY, CHANGE_FORM_PAY_BEFORCE_PAY_DAY, EPP1 , DD_SPLIT_ADITIONAL, DD_SPLIT_WITHOUT_ADITIONAL, PENDING_CONFIRMATION, FINAL_BILING
    
};

typedef NS_ENUM(NSInteger, bilingPeriod) {
    
    BILING_DUE_DATE_SCENARIO_1,  BILING_DUE_DATE_SCENARIO_2, DUE_DATE_NEXT_BILING_SCENARIO_1, DUE_DATE_NEXT_BILING_SCENARIO_2
    
};

@property (weak, nonatomic) IBOutlet UIView *modalView;
@property (weak, nonatomic) IBOutlet AdaptativeLabel *titleLabel;
@property (weak, nonatomic) IBOutlet AdaptativeTextview *infoTextView;
@property (weak, nonatomic) IBOutlet AdaptativeButton *linkButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linkButtonHeightConstraint;
@property(nonatomic, weak) id<NewModalViewDelegate>delegate;
@property(nonatomic, weak) id<BaseInfoSummaryCellProtocol>summaryDelegate;

@property validation modalValidation;
@property bilingPeriod modalBilingPeriod;

-(void) configureValidationModalWithOption:(int)option withText:(NSString*)text;
-(void) configureBilingPeriodModalWithOption: (int) option withText:(NSString*)text;

@end
