//
//  GraphicSummaryCell.h
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 07/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdaptativeLabel.h"
#import "GraphicView.h"

@interface GraphicSummaryCell : UITableViewCell<GraphicCellParentProtocol>

@property(weak, nonatomic) IBOutlet AdaptativeLabel* title;
@property(weak, nonatomic) IBOutlet UIView* contentGraphic;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint* heightContentGraphic;
@property(weak, nonatomic) IBOutlet UIView* lineMax;
@property(strong, nonatomic) GraphicView* g;
@property(assign, nonatomic) BOOL isAnimated;
@property(nonatomic, assign) BOOL beginAnimating;
@property(nonatomic, assign) BOOL endAnimating;

-(void) createGraphicView;
-(void)animate;

@end
