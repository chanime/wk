//
//  ContainerCell.m
//  celdas_hoy
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 02/01/2019.
//  Copyright © 2018 JOSE VICENTE GARCIA CHAHUAN. All rights reserved.
//
#import "ContainerCell.h"
#import "CreditAuxCell.h"

@implementation ContainerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.isShowed = false;
    self.haveScroll = false;
    //DEBUG ZONE
    self.ln.alpha = 0.0f;    
}

-(void)configure:(NSString *)desc withTitle:(NSString*)title withSubtitle:(NSString*)subtitle withImport:(NSString *)import highLight:(NSArray *)search{
    self.title.text = title;
    if( import != nil ){
        self.import.text = import;
        [self.import decreaseDecimal];
    }
    if( self.isShowed ){
        self.subtitle.hidden = false;
        self.iconSubtitle.hidden = false;
        self.subtitle.text = [subtitle stringByAppendingString:@"\n"];
        [self.subtitle lineHeight:lineHeightText];
    }
    else{
        self.subtitle.hidden = true;
        self.subtitle.text = @" ";
        self.iconSubtitle.hidden = true;
    }
    self.ln.text = desc;
    if( [search count] ){
        [self.title highlightText:[UIColor colorWithRed:240.0/255.0 green:181.0/255.0 blue:97.0/255.0 alpha:1] withSearch:search];
    }
}

-(IBAction)show:(id)sender{
    [super show:sender];
    [self.lineMax layoutIfNeeded];
    [self.delegate canShow:self];
}

@end
