//
//  GraphicSummaryCell.m
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 07/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import "GraphicSummaryCell.h"

@implementation GraphicSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.isAnimated = false;
    self.beginAnimating = false;
    self.endAnimating = false;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Create Graphic
-(void) createGraphicView{
    if( self.g != nil ){
        return;
    }
    self.g = (GraphicView*) [[[NSBundle mainBundle] loadNibNamed:@"GraphicView" owner:self options:nil] firstObject];
    self.g.frame = CGRectMake(0, 0, self.contentGraphic.frame.size.width, self.g.frame.size.height);
    self.heightContentGraphic.constant = self.g.frame.size.height;
    self.g.progress.frame = CGRectMake(0, 0, self.g.bar.frame.size.width, self.g.bar.frame.size.height);
    self.g.cellDelegate = self;
    [self.contentGraphic addSubview:self.g];
}

-(void)animate{
    if( self.isAnimated ){
        return;
    }
    self.beginAnimating = true;
    self.isAnimated = true;
    [self.g animatePoint];
}

#pragma mark - Graphic Cell Delegate
-(void)adviseForEndAnimation{
    [self setEndAnimating:true];
}

-(void)adviseForRepeatAnimation{
    self.beginAnimating = true;
    self.endAnimating = false;
}

@end
