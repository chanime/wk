//
//  AppManager.m
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 14/3/18.
//  Copyright © 2018 Everis. All rights reserved.
//

#import "AppManager.h"

@implementation AppManager

+(AppManager*)sharedManager{
    static dispatch_once_t pred;
    static AppManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[AppManager alloc] init];
        shared.isNewModeLoadExtractActive = YES;
        shared.weAreWaitingUploadImage = NO;
        shared.nUploadImages = 0;
        shared.selectedCard = nil;
        shared.billingExpensesResponse = nil;
    });
    return shared;
    
}
        
@end
