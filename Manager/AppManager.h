//
//  AppManager.h
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 14/3/18.
//  Copyright © 2018 Everis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SortedCard.h"
#import "BillingExpensesResponse.h"

@interface AppManager : NSObject

+(AppManager*) sharedManager;
    
@property(nonatomic, assign) BOOL isNewModeLoadExtractActive;
@property(nonatomic, assign) BOOL weAreWaitingUploadImage;
@property(nonatomic, assign) uint nUploadImages;
@property(nonatomic, strong) SortedCard* selectedCard;
@property(nonatomic, strong) BillingExpensesResponse* billingExpensesResponse;
    
@end
